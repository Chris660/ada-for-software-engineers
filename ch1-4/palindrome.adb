--
-- Create a palindrome from a string.
--

with Ada.Text_IO; use Ada.Text_IO;

procedure Palindrome is
   function Palin(S: in String) return String is
      T: String(1 .. ((2 * S'Length) - 1));
   begin
      T(1 .. S'Length) := S;

      -- Note: the central character in T will be overwritten.
      for N in S'Range loop
         T(T'Length - (N - S'First)) := S(N);
      end loop;

      return T;
   end Palin;
   
   S1: String := "a man a plan a c";
   S2: String := Palin(S1);
   S3: String(10..10 + ((2 * S1'Length) - 2)) := Palin(S1);

begin
   Put_Line(S1);
   Put_Line(S2);
   Put_Line(S3);
   Put_Line(Palin("able was I er"));
end Palindrome;

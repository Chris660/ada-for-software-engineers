package Root_Event.Telemetry is

   type Telemetry_Event is new Event with private;
   function Create return Telemetry_Event;
   procedure Simulate(E: in Telemetry_Event);

private

   type Subsystems is (Engines, Guidance, Comminications);
   type States is (OK, Failed);
   type Telemetry_Event is new Event with
      record
         ID: Subsystems;
         Status: States;
      end record;
end Root_Event.Telemetry;

--
-- Priority queue abstract data type implemented as a tree.
--
-- This is the most abstract version of the queue so far;
-- the client is completely shielded from the implementation
-- of the tree Node.
--
package Priority_Queue_Tree_V6 is
   type Queue(Size: Positive) is limited private;

   function Empty(Q: in Queue) return Boolean;
   procedure Put(Q: in out Queue; I: in  Integer);
   procedure Get(Q: in out Queue; I: out Integer);

   Overflow, Underflow: exception;

private
   -- The "visible" part above is unchanged relative to the array
   -- based implementation, so should be API compatible.
   type Node;                     -- completion in package body
   type Link is access Node;
   type Queue(Size: Positive) is
      record
         Root: Link;
      end record;
end Priority_Queue_Tree_V6;

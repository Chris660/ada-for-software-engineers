--
-- Priority queue abstract data type implemented as an array.
-- First attempt.
--
package Priority_Queue_V1 is
   function Empty return Boolean;
   procedure Put(I: in  Integer);
   procedure Get(I: out Integer);

   Overflow, Underflow: exception;
end Priority_Queue_V1;

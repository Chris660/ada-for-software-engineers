--
-- Priority queue abstract data type implemented as a tree.
--
package Priority_Queue_Tree_V4 is
   type Queue(Size: Positive) is limited private;

   function Empty(Q: in Queue) return Boolean;
   procedure Put(Q: in out Queue; I: in  Integer);
   procedure Get(Q: in out Queue; I: out Integer);

   Overflow, Underflow: exception;

private
   -- The "visible" part above is unchanged relative to the array
   -- based implementation, so should be API compatible.
   type Node;
   type Link is access Node;
   type Node is
      record
         Data: Integer;
         Left, Right: Link;
      end record;

   type Queue(Size: Positive) is
      record
         Root: Link;
      end record;
end Priority_Queue_Tree_V4;

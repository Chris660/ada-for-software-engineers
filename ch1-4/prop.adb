with Ada.Text_IO; use Ada.Text_IO;

procedure Prop is
   Ex: exception;

   procedure P1 is
      procedure P3 is
      begin
         raise Ex;
      end P3;

      procedure P2 is
      begin
         P3;
      exception
         when Ex => Put("Handled in P2");
      end P2;
   begin
      P2;
   exception
      when Ex => Put("Handled in P1");
   end P1;
begin
   P1;
end Prop;

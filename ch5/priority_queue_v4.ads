--
-- Priority queue abstract data type implemented as an array.
-- Forth attempt (the 3rd in the book does not compile).
--
package Priority_Queue_V4 is
   type Queue(Size: Positive) is private;

   function Empty(Q: in Queue) return Boolean;
   procedure Put(Q: in out Queue; I: in  Integer);
   procedure Get(Q: in out Queue; I: out Integer);

   Overflow, Underflow: exception;

private
   type Vector is array(Natural range <>) of Integer;
   type Queue(Size: Positive) is
      record
         Data: Vector(0 .. Size);
         Free: Natural := 0;
      end record;
end Priority_Queue_V4;

--
-- Read the manufacturer of a car and write the country
-- of origin of the car.
--

with Ada.Text_IO; use Ada.Text_IO;

procedure Country1 is
   type Cars is (
      Ford, Chevrolet, Pontiac, Chrysler, Dodge,
      Rover, Rolls_Royce,
      Peugeot, Renault, Citroen,
      BMW, Volkswagen, Opel,
      Honda, Mitsubishi, Toyota,
      Daewoo, Hyundai
   );

   type Countries is (USA, UK, France, Germany, Japan, Korea);

   function Car_To_Country(C: Cars) return Countries is
   begin
      case C is
         when Ford | Chevrolet | Pontiac | Chrysler | Dodge
                                  => return USA;
         when Rover | Rolls_Royce => return UK;
         when Peugeot .. Citroen  => return France;
         when BMW .. Opel         => return Germany;
         when Honda .. Toyota     => return Japan;
         when Daewoo | Hyundai    => return Korea;
      end case;
   end;

   S: String(1..80);
   Last: Integer;
   Car: Cars;

begin
   loop
      Put("Enter a make of car: ");
      Get_Line(S, Last);
      exit when Last = 0;

      Car := Cars'Value(S(1..Last));
      Put_Line(Cars'Image(Car) & " is made in "
               & Countries'Image(Car_To_Country(Car)));
   end loop;
end;

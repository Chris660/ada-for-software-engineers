--
-- Swap halves of an even length string.
--

with Ada.Text_IO; use Ada.Text_IO;

procedure Swap is
   S    : String  := "HelloWorld";
   Mid  : Natural := S'Length / 2;
   Temp : String  := S(1 .. Mid);
begin
   S(1 .. Mid) := S(Mid + 1 .. S'Length);
   S(Mid + 1 .. S'Length) := Temp;
   Put_Line(S);
end Swap;

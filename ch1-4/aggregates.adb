with Ada.Text_IO; use Ada.Text_IO;

procedure Aggregates is
   S1: String(1..5)  := (1..5 => '*');
   S2: String(1..5)  := (2..6 => '*');
   S3: String        := (1..5 => '*');
   -- S4: String        := (0..4 => '*'); -- String index type is Positive!
   S5: String(1..5)  := (others => '*');
   -- S6: String        := (others => '*'); -- Unable to determine bounds!
begin
   Put_Line(S1);
   Put_Line(S2);
   Put_Line(S3);
   -- Put_Line(S4);
   Put_Line(S5);
   -- Put_Line(S6);
end Aggregates;

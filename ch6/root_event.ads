package Root_Event is
   --
   -- Abstract base for all events
   --
   type Event is abstract tagged private;

   -- Primitive operations of all Events
   function Create return Event is abstract;
   procedure Simulate(E: in Event) is abstract;

   -- Comparison of events is common to all events in the "class".
   function "<"(Left, Right: Event'Class) return Boolean;

private

   subtype Simulation_Time is Integer range 0..10_000;
   type Event is abstract tagged
      record
         Time: Simulation_Time;
      end record;

end Root_Event;

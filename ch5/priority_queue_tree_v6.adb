package body Priority_Queue_Tree_V6 is
   type Node is -- completion of type declaration from package specification.
      record
         Data: Integer;
         Left, Right: Link;
      end record;

   --
   -- private interface - copied from ../ch1-4/progpqt.adb
   --
   procedure Put(Node_Ptr: in out Link; I: in Integer) is
   -- Descends Nodes recursively to locate the insertion position for I.
   begin
      if Node_Ptr = null then
         Node_Ptr := new Node'(I, null, null);
      elsif I < Node_Ptr.Data then
         Put(Node_Ptr.Left, I);
      else
         Put(Node_Ptr.Right, I);
      end if;
   end Put;

   procedure Get(Node_Ptr: in out Link; I: out Integer) is
   begin
      if Node_Ptr.Left = null then
         I        := Node_Ptr.Data;
         Node_Ptr := Node_Ptr.Right;
      else
         Get(Node_Ptr.Left, I);
      end if;
   end Get;

   --
   -- public interface.
   -- 
   function Empty(Q: in Queue) return Boolean is
   begin
      return Q.Root = null;
   end Empty;

   procedure Put(Q: in out Queue; I: Integer) is
   begin
      Put(Q.Root, I);
   exception
      when Storage_Error => raise Overflow;
   end Put;

   procedure Get(Q: in out Queue; I: out Integer) is
   begin
      if Q.Root = null then
         raise Underflow;
      else
         Get(Q.Root, I);
      end if;
   end Get;

end Priority_Queue_Tree_V6;

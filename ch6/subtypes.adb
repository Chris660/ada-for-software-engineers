with Ada.Text_IO;          use Ada.Text_IO;
with Ada.Integer_Text_IO;  use Ada.Integer_Text_IO;

procedure Subtypes is
   type Foo is new Integer range -32768..32767;
   subtype Bar is Foo range 0..100;

   X: Foo := 42;
   Y: Bar := 21;

   procedure Put_Foo(Val: in Foo) is
   begin
      Put_Line(Val'Image);
   end Put_Foo;

begin
   Put("Foo: ");
   Put_Foo(X);
   Put("Bar: ");
   Put_Foo(Y);
end Subtypes;

--
-- Priority queue abstract data type implemented as an array.
-- Second attempt.
--
package Priority_Queue_V2 is
   type Vector is array(Natural range <>) of Integer;
   type Queue(Size: Positive) is
      record
         Data: Vector(0 .. Size);
         Free: Natural := 0;
      end record;

   function Empty(Q: in Queue) return Boolean;
   procedure Put(Q: in out Queue; I: in  Integer);
   procedure Get(Q: in out Queue; I: out Integer);

   Overflow, Underflow: exception;
end Priority_Queue_V2;

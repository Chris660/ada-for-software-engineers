--
-- Read text from a file and write it filled and justified.
--
with Ada.Text_IO; use Ada.Text_IO;

procedure Justify is
   subtype Lines     is String(1..48);
   subtype Index     is Integer range 0..Lines'Last;

   -- Constant file name and margins
   File_Name:        constant String := "example.tex";
   Margin:           constant String(1..4) := (others => ' ');

   -- Compute number of characters in printed line.
   Width:            constant Index := Lines'Length - 2 * Margin'Length;

   Input:            File_Type;

   procedure Get_Word(
         Word:    out Lines;
         Length:  out Index;
         EOF:     out Boolean) is
      C: Character;
   begin
      Length := 0;
      EOF    := False;
      loop -- Skip leading ends-of-lines and blanks.
         if End_Of_File(Input) then
            EOF := True;
            return;
         elsif End_Of_Line(Input) then
            Skip_Line(Input);
         else
            Get(Input, C);
            exit when C /= ' ';
         end if;
      end loop;

      loop -- Read characters until space or EOL
         Length := Length + 1;
         Word(Length) := C;
         if Length > Width then
            -- Truncate word longer than line
            Skip_Line(Input);
            Length := Width;
            return;
         elsif End_Of_Line(Input) then
            Skip_Line(Input);
            return;
         end if;
         Get(Input, C);
         exit when C = ' ';
      end loop;
   end Get_Word;

   function Insert_Spaces(
      -- Insert extra spaces in output line
      Line:    Lines;   -- Current output line
      Length:  Index;   -- Length of current output line
      Words:   Index)   -- Number of words in line
      return   Lines    -- New justified line
   is
      Spaces:  Natural := Width - Length; -- Total extra spaces required.
      Gaps:    Natural := Words - 1;      -- Number of gaps between words.
      -- S is an array storing the number of spaces to add after each
      -- word.  S'Last is always 0.
      S: array(1..Words) of Natural := (others => 1 + (Spaces / Gaps));
      Buffer: Lines := (others => ' ');  -- New line buffer
      K1, K2: Index := 1;                -- Indices for copying
      L:      Index;                     -- Length of current word
   begin
      -- Distribute remaining spaces alternately left and right.
      for N in 1 .. Spaces mod Gaps loop
         if Ada.Text_IO.Line mod 2 = 1 then
            S(Words - N) := S(Words - N) + 1;
         else
            S(N) := S(N) + 1;
         end if;
      end loop;

      S(Words) := 0; -- No spaces after the last word.

      for W in 1 .. Words loop
         L := 1;
         while Line(K1 + L) /= ' ' loop
            L := L + 1;
         end loop;

         -- Copy word and extra spaces.
         Buffer(K2 .. K2 + L + S(W)) := Line(K1 .. K1 + L) & (1 .. S(W) => ' ');

         -- Advance indices past current work (and extra spaces).
         K1 := K1 + L + 1;
         K2 := K2 + L + S(W);
      end loop;

      return Buffer;
   end Insert_Spaces;

   procedure Put_Word(
      Word:          in Lines;         -- The word to insert
      Word_Length:   in Index;         -- The length of the word
      Words:         in out Index;     -- Current number of words
      Line:          in out Lines;     -- Output buffer
      Position:      in out Index) is  -- Output buffer index
   begin
      -- Note: position points past the trailing space.
      if Position - 1 + Word_Length > Width then
         if Words > 1 then
            -- Only insert spaces if we have multiple words on the line.
            Line := Insert_Spaces(Line, Position - 2, Words);
         end if;

         Put_Line(Margin & Line(1 .. Width));
         Line := (others => ' ');
         Position := 1;
         Words := 0;
      end if;

      -- Append word to line and update counters.
      Line(Position .. Position + Word_Length) := Word(1 .. Word_Length) & ' ';
      Position := Position + Word_Length + 1;
      Words := Words + 1;
   end Put_Word;

   procedure Main_Loop is
      Word:          Lines;                     -- Word buffer
      Word_Length:   Index;                     -- Buffer length
      EOF:           Boolean;                   -- True if EOF encountered
      Buffer:        Lines := (others => ' ');  -- Output line buffer
      Position:      Index := 1;                -- Next position to insert
      Word_Count:    Index := 0;                -- Number of words
   begin
      loop
         Get_Word(Word, Word_Length, EOF);
         exit when EOF;
         Put_Word(Word, Word_Length, Word_Count, Buffer, Position);
      end loop;
      Put_Line(Margin & Buffer(1 .. Position - 1)); -- Flush last line
   end Main_Loop;
begin
   Open(Input, In_File, File_Name);
   Main_Loop;
   Close(Input);
end Justify;

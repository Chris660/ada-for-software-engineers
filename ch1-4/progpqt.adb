--
-- Tree based priority queue.
--
with Ada.Text_IO;          use Ada.Text_IO; 
with Ada.Integer_Text_IO;  use Ada.Integer_Text_IO;

procedure ProgPQT is
   type Node; -- fwd declaration?
   type Link is access Node;
   type Node is
      record
         Data: Integer;
         Left, Right: Link;
      end record;

   type Queue is
      record
         Root: Link;
      end record;

   Overflow, Underflow: exception;

   function Is_Empty(Q: in Queue) return Boolean is
   begin
      return Q.Root = null;
   end Is_Empty;

   procedure Put(Node_Ptr: in out Link; I: in Integer) is
   -- Descends Nodes recursively to locate the insertion position for I.
   begin
      if Node_Ptr = null then
         Node_Ptr := new Node'(I, null, null);
      elsif I < Node_Ptr.Data then
         Put(Node_Ptr.Left, I);
      else
         Put(Node_Ptr.Right, I);
      end if;
   end Put;

   procedure Put(Q: in out Queue; I: Integer) is
   begin
      Put(Q.Root, I);
   exception
      when Storage_Error => raise Overflow;
   end Put;


   procedure Get(Node_Ptr: in out Link; I: out Integer) is
   begin
      if Node_Ptr.Left = null then
         I        := Node_Ptr.Data;
         Node_Ptr := Node_Ptr.Right;
      else
         Get(Node_Ptr.Left, I);
      end if;
   end Get;

   procedure Get(Q: in out Queue; I: out Integer) is
   begin
      if Q.Root = null then
         raise Underflow;
      else
         Get(Q.Root, I);
      end if;
   end Get;

   Q: Queue;
   I: Integer;
   Test_Data: array(Positive range <>) of Integer :=
      (10, 5, 0, 25, 15, 30, 15, 20, -6, 40);

begin
   Put(" In: ");
   for N in Test_Data'Range loop
      Put(Test_Data(N), Width => 5);
      Put(Q, Test_Data(N));
   end loop;
   New_Line;

   Put("Out: ");
   while not Is_Empty(Q) loop
      Get(Q, I);
      Put(I, Width => 5);
   end loop;
   New_Line;

   Get(Q, I); -- test underflow

exception
   when Underflow => Put_Line("Queue underflow"); -- oom
   when Overflow  => Put_Line("Queue overflow");
end ProgPQT;

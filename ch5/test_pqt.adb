with Priority_Queue_Tree_V6;
with Ada.Text_IO;         use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;

procedure Test_PQT is
   Pad       : constant Natural := 5;
   I         : Integer;
   Test_Data : array(Positive range <>) of Integer :=
      (10, 5, 0, 25, 15, 30, 20, -6, 40);

   package Priority_Queue renames Priority_Queue_Tree_V6;

   Q: Priority_Queue.Queue(100);

begin
   Put(" in:");
   for N in Test_Data'Range loop
      Put(Test_Data(N), Width => Pad);
      Priority_Queue.Put(Q, Test_Data(N));
   end loop;
   New_Line;

   Put("out:");
   while not Priority_Queue.Empty(Q) loop
      Priority_Queue.Get(Q, I);
      Put(I, Width => Pad);
   end loop;
   New_Line;

   Priority_Queue.Get(Q, I); -- test underflow

exception
   when Priority_Queue.Underflow => Put_Line("caught queue underflow");
   when Priority_Queue.Overflow  => Put_Line("caught queue overflow");
end Test_PQT;

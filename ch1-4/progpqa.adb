with Ada.Text_IO;         use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;

procedure ProgPQA is
   type Vector is array(Natural range <>) of Integer;
   type Queue(Size: Positive) is
      record
         Data : Vector(0 .. Size); -- note: extra slot for sentinal value.
         Free : Natural := 0;
      end record;

   Overflow, Underflow: exception;

   function Is_Empty(Q: Queue) return Boolean is
   begin
      return Q.Free = 0;
   end Is_Empty;

   procedure Put(Q: in out Queue; I: in Integer) is
      Index: Integer range Q.Data'Range := 0;
   begin
      if Q.Free = Q.Size then
         raise Overflow;
      end if;
      
      -- Sentinal search for place to insert.
      Q.Data(Q.Free) := I;
      while Q.Data(Index) < I loop
         Index := Index + 1;
      end loop;

      -- Move elements to free space for the new element.
      if Index < Q.Free then
         Q.Data(Index + 1 .. Q.Free) := Q.Data(Index .. Q.Free - 1);
      end if;

      Q.Data(Index) := I;
      Q.Free := Q.Free + 1;
   end Put;

   function Get(Q: in out Queue) return Integer is
   begin
      if Q.Free = 0 then
         raise Underflow;
      end if;

      return I: Integer do
         I := Q.Data(0);
         Q.Free := Q.Free - 1;
         Q.Data(0 .. Q.Free - 1) := Q.Data(1.. Q.Free);
      end return;
   end Get;

   Q: Queue(10);
   I: Integer;
   Test_Data: array(Positive range <>) of Integer :=
      (10, 5, 0, 25, 15, 30, 15, 20, -6, 40);

begin
   Put(" In: ");
   for N in Test_Data'Range loop
      Put(Test_Data(N), Width => 5); -- Print the inputs
      Put(Q, Test_Data(N));
   end loop;

   New_Line;
   -- Put(Q, 17); -- test overflow

   Put("Out: ");
   while not Is_Empty(Q) loop
      I := Get(Q);
      Put(I, Width => 5);
   end loop;

   New_Line;
   -- I := Get(Q); -- test underflow

exception
   when Underflow => Put_Line("Queue underflow");
   when Overflow  => Put_Line("Queue overflow");
end ProgPQA;
